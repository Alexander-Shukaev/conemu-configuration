#!/usr/bin/env python
# encoding: utf-8

# Preamble {{{
# ==============================================================================
#       @file cec-msys2-zsh.py
# ------------------------------------------------------------------------------
#     @author Alexander Shukaev <http://Alexander.Shukaev.name>
# ------------------------------------------------------------------------------
# @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
# ------------------------------------------------------------------------------
#  @copyright Copyright (C) 2014,
#             Alexander Shukaev <http://Alexander.Shukaev.name>.
#             All rights reserved.
# ------------------------------------------------------------------------------
#    @license This program is free software: you can redistribute it and/or
#             modify it under the terms of the GNU General Public License as
#             published by the Free Software Foundation, either version 3 of the
#             License, or (at your option) any later version.
#
#             This program is distributed in the hope that it will be useful,
#             but WITHOUT ANY WARRANTY; without even the implied warranty of
#             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#             General Public License for more details.
#
#             You should have received a copy of the GNU General Public License
#             along with this program. If not, see
#             <http://www.gnu.org/licenses/>.
# ==============================================================================
# }}} Preamble

# Imports {{{
# ==============================================================================
import argparse
import itertools
import os
import subprocess
# ==============================================================================
# }}} Imports

# Constants {{{
# ==============================================================================
KEYS = ('HKEY_CLASSES_ROOT\Directory\shell',
        'HKEY_CLASSES_ROOT\Directory\Background\shell',
        'HKEY_CLASSES_ROOT\Drive\shell')
# ------------------------------------------------------------------------------
MENUS = ('MSYS2 Zsh',
         'MSYS2 Zsh (Administrator)')
# ==============================================================================
# }}} Constants

# Functions {{{
# ==============================================================================
# Public {{{
# ==============================================================================
def install(args):
  if not 'ConEmu_EXECUTABLE' in os.environ:
    raise Exception("Environment variable does not exist:"
                    ' '
                    "\"ConEmu_EXECUTABLE\".")

  conemu_executable = '%ConEmu_EXECUTABLE%'

  if not 'ConEmu_CONFIGURATION' in os.environ:
    raise Exception("Environment variable does not exist:"
                    ' '
                    "\"ConEmu_CONFIGURATION\".")

  conemu_configuration = '%ConEmu_CONFIGURATION%'

  if not 'MSYS2_BINARY_DIR' in os.environ:
    raise Exception("Environment variable does not exist:"
                    ' '
                    "\"MSYS2_BINARY_DIR\".")

  msys2_binary_dir = '%MSYS2_BINARY_DIR%'

  if not 'MSYS2_ICON' in os.environ:
    raise Exception("Environment variable does not exist:"
                    ' '
                    "\"MSYS2_ICON\".")

  msys2_icon = '%MSYS2_ICON%'

  for key, menu in itertools.product(KEYS, MENUS):
    key = key + '\\' + menu

    subprocess.call(('reg.exe', 'add',
                     key,
                     '/ve',
                     '/d', menu,
                     '/f'))

    subprocess.call(('reg.exe', 'add',
                     key,
                     '/v', 'Icon',
                     '/t', 'REG_EXPAND_SZ',
                     '/d', msys2_icon,
                     '/f'))

    command = ('"'                                                             +
               conemu_executable                                               +
               '"'                                                             +
               ' '                                                             +
               '/LoadCfgFile'                                                  +
               ' '                                                             +
               '"'                                                             +
               conemu_configuration                                            +
               '"'                                                             +
               ' '                                                             +
               '/Dir'                                                          +
               ' '                                                             +
               '"'                                                             +
               '%V'                                                            +
               '"'                                                             +
               ' '                                                             +
               '/cmd'                                                          +
               ' '                                                             +
               '"'                                                             +
               os.path.join(msys2_binary_dir, 'zsh.exe')                       +
               '"'                                                             +
               ' '                                                             +
               '-l'                                                            +
               ' '                                                             +
               '-c'                                                            +
               ' '                                                             +
               '"'                                                             +
               'cd \\"%V\\"; zsh -i'                                           +
               '"'                                                             +
               ' '                                                             +
               '"'                                                             +
               '-new_console'                                                  +
               ':'                                                             +
               ('a' if menu == MENUS[1] else '')                               +
               'n'                                                             +
               'C'                                                             +
               ':'                                                             +
               msys2_icon                                                      +
               '"')

    subprocess.call(('reg.exe', 'add',
                     key + '\command',
                     '/ve',
                     '/t', 'REG_EXPAND_SZ',
                     '/d', command,
                     '/f'))
# ------------------------------------------------------------------------------
def uninstall(args):
  for key, menu in itertools.product(KEYS, MENUS):
    key = key + '\\' + menu

    subprocess.call(('reg.exe', 'delete',
                     key,
                     '/f'))
# ------------------------------------------------------------------------------
def main():
  parser     = argparse.ArgumentParser()
  subparsers = parser.add_subparsers()

  install_parser = subparsers.add_parser('install')
  install_parser.set_defaults(func=install)

  uninstall_parser = subparsers.add_parser('uninstall')
  uninstall_parser.set_defaults(func=uninstall)

  args = parser.parse_args()
  args.func(args)
# ==============================================================================
# }}} Public
# ==============================================================================
# }}} Functions

if __name__ == '__main__':
  main()

# Modeline {{{
# ==============================================================================
# vim:ft=python:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
# ==============================================================================
# }}} Modeline
