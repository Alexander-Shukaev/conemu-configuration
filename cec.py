#!/usr/bin/env python
# encoding: utf-8

# Preamble {{{
# ==============================================================================
#       @file cec.py
# ------------------------------------------------------------------------------
#     @author Alexander Shukaev <http://Alexander.Shukaev.name>
# ------------------------------------------------------------------------------
# @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
# ------------------------------------------------------------------------------
#  @copyright Copyright (C) 2014,
#             Alexander Shukaev <http://Alexander.Shukaev.name>.
#             All rights reserved.
# ------------------------------------------------------------------------------
#    @license This program is free software: you can redistribute it and/or
#             modify it under the terms of the GNU General Public License as
#             published by the Free Software Foundation, either version 3 of the
#             License, or (at your option) any later version.
#
#             This program is distributed in the hope that it will be useful,
#             but WITHOUT ANY WARRANTY; without even the implied warranty of
#             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#             General Public License for more details.
#
#             You should have received a copy of the GNU General Public License
#             along with this program. If not, see
#             <http://www.gnu.org/licenses/>.
# ==============================================================================
# }}} Preamble

# Imports {{{
# ==============================================================================
import argparse
import subprocess
# ==============================================================================
# }}} Imports

# Functions {{{
# ==============================================================================
# Public {{{
# ==============================================================================
def install(args):
  subprocess.call(('cec-command-prompt.py', 'install'), shell=True)
  subprocess.call(('cec-cygwin-bash.py',    'install'), shell=True)
  subprocess.call(('cec-msys2-bash.py',     'install'), shell=True)
  subprocess.call(('cec-msys2-zsh.py',      'install'), shell=True)
# ------------------------------------------------------------------------------
def uninstall(args):
  subprocess.call(('cec-command-prompt.py', 'uninstall'), shell=True)
  subprocess.call(('cec-cygwin-bash.py',    'uninstall'), shell=True)
  subprocess.call(('cec-msys2-bash.py',     'uninstall'), shell=True)
  subprocess.call(('cec-msys2-zsh.py',      'uninstall'), shell=True)
# ------------------------------------------------------------------------------
def main():
  parser     = argparse.ArgumentParser()
  subparsers = parser.add_subparsers()

  install_parser = subparsers.add_parser('install')
  install_parser.set_defaults(func=install)

  uninstall_parser = subparsers.add_parser('uninstall')
  uninstall_parser.set_defaults(func=uninstall)

  args = parser.parse_args()
  args.func(args)
# ==============================================================================
# }}} Public
# ==============================================================================
# }}} Functions

if __name__ == '__main__':
  main()

# Modeline {{{
# ==============================================================================
# vim:ft=python:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
# ==============================================================================
# }}} Modeline
